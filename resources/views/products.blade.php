<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            Products
        </h2>
    </x-slot>
    <div class="py-12">
        <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="card">
                    @if(session('success'))
                    <div class="alert alert-warning alert-dismissible fade show" role="alert">
                        <strong>{{ session('success') }}</strong> 
                        <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="card-header"> 
                    Products
                    </div>
                </div>
                <table class="table">
                    <thead>
                        <tr>
                        <th scope="col">sr no</th>
                        <th scope="col">Name</th>
                        <th scope="col">Price</th>
                        <th scope="col">UPC</th>
                        <th scope="col">status</th>

                        </tr>
                    </thead>
                    <tbody>
                        @php($i=1)                       
                         @foreach ($products as $product)
                        <tr>
                        <th scope="row">{{$i++}}</th>
                        <td>{{$product->name}}</td>
                        <td>{{$product->price}}</td>
                        <td>{{$product->upc}}</td>
                        <td>{{$product->status}}</td>

                        </tr>
                        @endforeach
                        
                    </tbody>
                </table>
            </div>
            <div class="col-md-4">
                <div class="card">
                    <div class="card-header"> 
                    Add Product
                    </div>
                </div>
                <form action="{{ route('insertProduct') }}" method="post">
                   @csrf
                    <div class="form-group">
                        <label for="exampleInputEmail1">Name</label>
                        <input type="text" class="form-control" id="" name="name">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">Price</label>
                        <input type="number" class="form-control" name="price">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">UPC</label>
                        <input type="text" class="form-control" name="upc">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputPassword1">image</label>
                        <input type="file" class="form-control" name="pic">
                    </div>
                    <button type="submit" class="btn btn-primary">Add</button>
                </form>
            </div>
        </div>
</div>
</div>


</x-app-layout>