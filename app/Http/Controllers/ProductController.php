<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Products;
use Auth;
use Illuminate\Support\Carbon;


class ProductController extends Controller
{
    public function showProduct()
    {
        $products= Products::all();     
        return view('products', compact('products'));
    }
    public function insertProduct(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|unique:name|max:255',
            'price' => 'required',
        ]);
        $product =new Products;
        $product->name = $request->name;
        $product->price = $request->price;
        $product->upc = $request->upc;
        $product->status = 'available';
        $product->save();

        return Redirect()->back()->with('success','product added successfully');



    }
}
